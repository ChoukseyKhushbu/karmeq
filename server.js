const express = require("express");
const app = express();
const authRouter = require("./routes/auth.js");
const userDashboardPrefrencesRouter = require("./routes/userDashboardPrefrences");
const orderRouter = require("./routes/orders");
const transferRouter = require("./routes/transfer");
const portfolioRouter = require("./routes/portfolio");
const newsRouter = require("./routes/news");
const messageRouter = require("./routes/message");
const investmentRouter = require("./routes/investment");
const esgRouter = require("./routes/esg");
const wishlistRouter = require("./routes/wishlist");
const giftRouter = require("./routes/gift");
const giftingRouter = require("./routes/gifting");
const giftHistoryRouter = require("./routes/giftHistory");
const watchlistRouter = require("./routes/watchlist");
const marketRouter = require("./routes/market");
app.use(express.json());

app.use("/auth", authRouter);
app.use("/user", userDashboardPrefrencesRouter);
app.use("/order", orderRouter);
app.use("/transfer", transferRouter);
app.use("/portfolio", portfolioRouter);

app.use("/gift", giftRouter);
app.use("/gifting", giftingRouter);
app.use("/gift-history", giftHistoryRouter);

app.use("/news", newsRouter);
app.use("/esg", esgRouter);
app.use("/wishlist", wishlistRouter);
app.use("/message", messageRouter);
app.use("/investment-account", investmentRouter);
app.use("/watchlist", watchlistRouter);
app.use("/market", marketRouter);

try {
  app.listen(8000, () => {
    console.log("server started on port 8000");
  });
} catch (err) {
  console.log(err, "error in listening to port");
}
