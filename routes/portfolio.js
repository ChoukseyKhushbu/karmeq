const express = require("express");
const Router = express.Router();
const Portfolio = require("../MockData/Portfolio.json");

Router.get("/", (req, res) => {
  res.json(Portfolio.portfolio);
});
Router.get("/chart", (req, res) => {
  let { chart } = req.body;
  res.json("displaying chart");
});
Router.get("/movers", (req, res) => {
  let { exchange, interval } = req.body;
  res.json({
    high: [],
    low: [],
  });
});
Router.get("/stock/:STOCK", (req, res) => {
  res.json(Portfolio.stock);
});
module.exports = Router;
