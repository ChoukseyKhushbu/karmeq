const express = require("express");
const Router = express.Router();

const Message = require("../MockData/Message.json");
Router.get("/:id", (req, res) => {
  const result = Message.message.filter((m) => m.id == req.params.id);
  res.json(result);
});
Router.get("/", (req, res) => {
  res.json(Message.message);
});
module.exports = Router;
