const express = require("express");
const Router = express.Router();
const Portfolio = require("../MockData/Portfolio.json");
const Market = require("../MockData/Market.json");

Router.get("/chart", (req, res) => {
  res.json(Market.stocks);
});
Router.get("/movers", (req, res) => {
  res.json({
    high: Market.stocks,
    low: Market.stocks,
  });
});

Router.get("/stock/:STOCK", (req, res) => {
  res.json(Portfolio.stock);
});

module.exports = Router;
