const express = require("express");
const Router = express.Router();

Router.post("/signup", (req, res) => {
  let response = req.body;
  res.status(200).json(response);
});

Router.post("/signup/validate", (req, res) => {
  let { mobile, otp } = req.body;
  if (mobile) {
    if (otp === 1234) {
      res.status(200).json(true);
    } else {
      res.status(400).json(false);
    }
  } else {
    res.status(400).json("moblie required");
  }
});

Router.post("/signin", (req, res) => {
  let { email, password } = req.body;
  if (email === "abc@1.com") {
    if (password === "test") {
      res.status(200).json(`succesfully signed in as ${email}`);
    } else {
      res.status(400).json("wrong password");
    }
  } else {
    res.status(400).json("Email not found");
  }
});

Router.post("/signin/validate", (req, res) => {
  let { email, otp } = req.body;
  if (email) {
    if (otp === 1234) {
      res.status(200).json(true);
    } else {
      res.status(400).json(false);
    }
  } else {
    res.status(400).json("please provide an email");
  }
});

module.exports = Router;
