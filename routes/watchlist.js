const express = require("express");
const Router = express.Router();

Router.post("/", (req, res) => {
  res.json({
    stocks: [],
  });
});
Router.delete("/", (req, res) => {
  res.json("deleted");
});
module.exports = Router;
