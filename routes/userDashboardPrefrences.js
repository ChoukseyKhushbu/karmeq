const express = require("express");
const marketPositionsMockdata = require("../MockData/MarketPositionsMockdata.json");
const UserProfileData = require("../MockData/UserProfileData.json");

const Router = express.Router();

Router.post("/profile", (req, res) => {
  let { experienced, objective, stocks_type } = req.body;
  try {
    UserProfileData.experienced =
      experienced !== undefined ? experienced : UserProfileData.experienced;

    UserProfileData.objective =
      objective !== undefined ? objective : UserProfileData.objective;

    UserProfileData.stocks_type =
      stocks_type !== undefined ? stocks_type : UserProfileData.stocks_type;

    res.status(200).json(UserProfileData);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
Router.get("/profile", (req, res) => {
  res.status(200).json(UserProfileData);
});

Router.get("/market-positions", (req, res) => {
  res.status(200).json(marketPositionsMockdata);
});
module.exports = Router;
