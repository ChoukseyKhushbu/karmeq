const express = require("express");
const Router = express.Router();
const Order = require("../MockData/Order.json");

Router.get("/", (req, res) => {
  res.status(200).json(Order.orders);
});
Router.get("/:orderID", (req, res) => {
  let { orderID } = req.params;
  const result = Order.orders.filter((ord) => ord.order_id === orderID);
  if (result.length) {
    res.json(result);
  } else {
    res.json("order not found");
  }
});
module.exports = Router;
