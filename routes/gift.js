const express = require("express");
const Router = express.Router();
const Gifts = require("../MockData/Gifts.json");

Router.get("/:giftID", (req, res) => {
  const { giftID } = req.params;
  res.json(Gifts);
});
Router.post("/thank-you", (req, res) => {
  let { gift_id, message } = req.body;
  res.json(message);
});
module.exports = Router;
