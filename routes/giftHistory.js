const express = require("express");
const Router = express.Router();
const Gifts = require("../MockData/Gifts.json");
Router.get("/", (req, res) => {
  res.json(Gifts);
});
module.exports = Router;
