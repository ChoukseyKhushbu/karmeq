const express = require("express");
const Router = express.Router();
const Transfer = require("../MockData/Transfer.json");
Router.get("/:id", (req, res) => {
  res.json(Transfer);
});
module.exports = Router;
