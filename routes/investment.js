const express = require("express");
const Router = express.Router();

Router.post("/", (req, res) => {
  res.json(req.body);
});
module.exports = Router;
