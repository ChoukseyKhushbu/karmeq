const express = require("express");
const Router = express.Router();
const ESG = require("../MockData/ESG.json");

Router.get("/", (req, res) => {
  let { type, exchange, interval } = req.body;
  res.json({
    stocks: ESG.stocks,
    interval: "-",
  });
});
Router.get("/stock/:STOCK", (req, res) => {
  const result = ESG.stocks.find((s) => s.stock === req.params.STOCK);
  if (result) {
    res.json(result);
  } else {
    res.json("stock not found");
  }
});

Router.get("/:STRING_SEARCH", (req, res) => {
  let { type, exchange, interval } = req.body;
  const result = ESG.stocks.find((s) => s.stock === req.params.STRING_SEARCH);
  if (result) {
    res.json(result);
  } else {
    res.json("stock not found");
  }
});

module.exports = Router;
