const express = require("express");
const Router = express.Router();

const News = require("../MockData/News.json");
Router.get("/", (req, res) => {
  res.json(News);
});
module.exports = Router;
