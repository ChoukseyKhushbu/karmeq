const express = require("express");
const Gifting = require("../MockData/Gifting.json");
const Router = express.Router();

Router.post("/", (req, res) => {
  res.json(req.body);
});

Router.get("/is-gifting-allowed/:STATE", (req, res) => {
  let { STATE } = req.params;
  let status = STATE.toLocaleUpperCase() === "USA" ? true : false;
  res.json(status);
});
module.exports = Router;
