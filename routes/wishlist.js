const express = require("express");
const Router = express.Router();
const Stock = require("../MockData/Stock.json");

Router.get("/", (req, res) => {
  res.json(Stock.stocks);
});
Router.post("/", (req, res) => {
  res.json(req.body);
});
Router.delete("/", (req, res) => {
  res.json("post deleted");
});
module.exports = Router;
